/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.duplicatezeros;

import java.util.Arrays;

/**
 *
 * @author User
 */
public class DuplicateZeros {
    public static void duplicateZeros(int[] arr) {
        int possibleDups = 0;
        int length = arr.length - 1;

        
        for (int i = 0; i <= length - possibleDups; i++) {
            if (arr[i] == 0) {
                
                if (i == length - possibleDups) {
                    arr[length] = 0;
                    length--;
                    break;
                }
                possibleDups++;
            }
        }

        
        int last = length - possibleDups;
        for (int i = last; i >= 0; i--) {
            if (arr[i] == 0) {
                arr[i + possibleDups] = 0;
                possibleDups--;
                arr[i + possibleDups] = 0;
            } else {
                arr[i + possibleDups] = arr[i];
            }
        }
    }

    public static void main(String[] args) {
        int[] arr = {1,0,2,3,0,4,5,0 };
        duplicateZeros(arr);
        System.out.println("Modified Array: " + Arrays.toString(arr));
    }
}
